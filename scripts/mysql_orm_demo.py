import os
import sys

ROOT = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../")
sys.path.append(ROOT)

from appservice.support.mysql_base import MysqlBase
from appservice.mysql_connection import get_test_mysql_db
from sqlalchemy.engine.result import ResultProxy
from sqlalchemy.orm import sessionmaker, Session

"""
mysql 访问demo
@date 2018-08-02
@author zhouwei
"""


class Test(MysqlBase):
    __session: Session = sessionmaker(bind=get_test_mysql_db())

    __slots__ = (
        "id",
        "name",
        "create_datetime",
        "update_datetime",
    )

    def __init__(self):
        pass

    @classmethod
    def get_by_recommend_type(cls, id):
        sql = "select * from test where id = :id"
        params = dict()
        params["id"] = id
        session = Test.__session()
        result: ResultProxy = session.execute(sql, params)
        session.close()
        return super().to_list_object(cls, result)

    @classmethod
    def delete(cls, id):
        sql = "update test set name = :name where id = :id"
        params = dict()
        params["id"] = id
        params["name"] = "hello word"
        session = Test.__session()
        result: ResultProxy = session.execute(sql, params)
        session.commit()
        session.close()
        return result.rowcount

    @classmethod
    def insert(cls, flux_type_id, recommend_type, subject):
        sql = """
            insert into test (name, create_datetime, update_datetime)
            values (:name, 0, now(), now())
        """
        params = dict()
        params["name"] = "world"
        session = Test.__session()
        result: ResultProxy = session.execute(sql, params)
        session.commit()
        session.close()
        return result.lastrowid
