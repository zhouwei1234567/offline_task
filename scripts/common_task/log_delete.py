import os
import sys
import traceback
import shutil

ROOT = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../")
sys.path.append(ROOT)

from appservice import logger_dir
from appservice.utils import time_units

# 日志地址
log_path = logger_dir

'''
日志删除脚本
'''
if __name__ == "__main__":
    delete_timestamp = time_units.now_timestamp() - 7 * 24 * 60 * 60 * 1000
    for name in os.listdir(log_path):
        path_tmp = log_path + '/' + name
        if os.path.isdir(path_tmp):
            try:
                timestamp = time_units.format_date_string_to_timestamp('%Y%m%d', name)
                if timestamp <= delete_timestamp:
                    shutil.rmtree(path_tmp)
                    print("delete: {}".format(path_tmp))
            except BaseException as be:
                print(traceback.format_exc())

