import time
import datetime
import subprocess
import os
import sys
import traceback

ROOT = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../")
sys.path.append(ROOT)

from appservice import get_data_dir, logger_dir_date, get_logger_new
from scripts.online_school_model_data.data_object import DataObject

'''
定期将脚本设置有可执行的权限
'''
if __name__ == "__main__":
    scripts_dir = os.path.join(ROOT, 'scripts')
    p = subprocess.Popen('chmod +x -R {}'.format(scripts_dir), shell=True)
    print('chmod +x -R {}'.format(scripts_dir))
    p.wait()