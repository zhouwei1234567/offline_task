import os
import sys

ROOT = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../")
sys.path.append(ROOT)

from appservice.mongo_connection import create_mongo_test_db

"""
mongo 访问demo
@date 2018-08-02
@author zhouwei
"""

test_db = create_mongo_test_db()

for info in test_db['db']['collection'].find():
    print(info)

new_collection = {
    '_id': '123',
    'name': 'hello word'
}
test_db['db']['collection'].update_one(filter={"_id": new_collection['_id']}, update={"$set": new_collection}, upsert=True)
