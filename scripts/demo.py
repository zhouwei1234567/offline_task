import os
import sys

ROOT = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../../")
sys.path.append(ROOT)

'''
1. 上面4行，每个脚本必须加上， 若脚本是在scripts/xxx/xxx，则/../..需要变为/../../../
2. 加入sys.path.append，是因为引用from appservice，需要将其加入到path中，否则引用不到
@date 2018-09-02
@author zhouwei
'''

from appservice import mongo_connection
from appservice import redis_connection
from appservice import mysql_connection
from appservice import logger
from appservice import get_logger_new, get_data_dir

# 初始化mongo的链接
test_mongo_db = mongo_connection.create_mongo_test_db(1, 0)

print(test_mongo_db)

# 初始化redis的链接
test_redis = redis_connection.create_test_cluster(1)
print(test_redis)

# 初始化mysql的链接
test_mysql_db = mysql_connection.get_test_mysql_db()
print(test_mysql_db)

logger.error("error")
logger.warn("warn")
logger.info("info")
logger.debug("debug")

# 生成自己的log文件，该文件会在APP_ROOT_DIR生成exam_report的文件夹，并在下面新建import_exam_answer名字的log文件，存放使用_logger打印的日志
_logger = get_logger_new("exam_report", "import_exam_answer")

# 开始编写正常的定时任务处理脚本，就是这些了

#
