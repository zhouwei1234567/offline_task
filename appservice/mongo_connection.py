from pymongo import MongoClient
from . import app_config

__mongo_db_cache = {}


# 公用的创建mongodb的工厂
def create_mongo_db_factory(
    mongo_name, max_pool_size, min_pool_size, shard, is_use_cache=True
):
    key = mongo_name
    if shard is not None:
        key = key + "-" + str(shard)

    if is_use_cache and __mongo_db_cache.get(key) is not None:
        return __mongo_db_cache.get(key)

    mongodb_config = app_config.mongo_config[key]
    if max_pool_size is None:
        max_pool_size = mongodb_config.max_pool_size
    if min_pool_size is None:
        min_pool_size = mongodb_config.min_pool_size
    mongo = MongoClient(
        host=mongodb_config(),
        username=mongodb_config.username,
        password=mongodb_config.password,
        maxPoolSize=max_pool_size,
        minPoolSize=min_pool_size,
        maxIdleTimeMS=mongodb_config.max_idle_time_ms,
    )
    if is_use_cache and mongo is not None:
        __mongo_db_cache[key] = mongo
    return mongo


# 创建test的mongodb
def create_mongo_test_db(
    max_pool_size=None, min_pool_size=None, shard=None, is_use_cache=True) -> MongoClient:
    return create_mongo_db_factory(
        "test", max_pool_size, min_pool_size, shard, is_use_cache=is_use_cache
    )

