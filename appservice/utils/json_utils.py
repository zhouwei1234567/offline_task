from json import loads, dumps


def json_loads(s, encoding=None, cls=None, object_hook=None, parse_float=None,
          parse_int=None, parse_constant=None, object_pairs_hook=None, **kw):
    return loads(s, encoding=encoding, cls=cls, object_hook=object_hook, parse_float=parse_float,
                      parse_int=parse_int, parse_constant=parse_constant, object_pairs_hook=object_pairs_hook, **kw)


def json_dumps(obj, *, skipkeys=False, ensure_ascii=False, check_circular=True, allow_nan=True, cls=None, indent=None,
          separators=None, default=lambda o: o.__dict__, sort_keys=False, **kw):

    return dumps(obj, skipkeys=skipkeys, ensure_ascii=ensure_ascii, check_circular=check_circular,
                      allow_nan=allow_nan, cls=cls, indent=indent, separators=separators,
                      default=default, sort_keys=sort_keys, **kw)

