import time


# 获取当前的时间戳
def now_timestamp():
    return int(time.time() * 1000)


# 获取当前yyyy-MM-dd HH:mm:ss的格式化日志
def now_datetime_string():
    local_time = time.localtime(time.time())
    return str(time.strftime("%Y-%m-%d %H:%M:%S", local_time))


# 获取当前yyyy-MM-dd的格式化日志
def now_date_string():
    local_time = time.localtime(time.time())
    return str(time.strftime("%Y-%m-%d", local_time))


# 将yyyy-MM-dd HH:mm:ss转换成毫秒的时间戳
def datetime_string_to_timestamp(datetime):
    local_time = time.strptime(datetime, "%Y-%m-%d %H:%M:%S")
    return int(time.mktime(local_time) * 1000)


# 将yyyy-MM-dd转换成毫秒的时间戳
def date_string_to_timestamp(date):
    local_time = time.strptime(date, "%Y-%m-%d")
    return int(time.mktime(local_time) * 1000)


# 将一定格式的日志，转换成毫秒时间戳
def format_date_string_to_timestamp(date_format, date):
    local_time = time.strptime(date, date_format)
    return int(time.mktime(local_time) * 1000)

# 将datetime转成毫秒数
def datetime_to_timestamp(date):
    return time.mktime(date.timetuple()) * 1000

# 将一定格式的日志，转换成时间戳
# timestamp为毫秒的时间戳
def format_timestamp_to_date_string(date_format, timestamp):
    """

    :rtype:
    """
    return str(time.strftime(date_format, time.localtime(timestamp / 1000)))


# 获取当前学期
def get_this_term():
    now_time = time.time()
    now_time_timestamp = int(now_time * 1000)
    local_time = time.localtime(now_time)
    now_year = local_time.tm_year
    pre_year = local_time.tm_year - 1
    next_year = local_time.tm_year + 1
    now_year_31 = str(now_year) + '-02-13'
    now_year_91 = str(now_year) + '-08-10'
    now_year_31_timestamp = date_string_to_timestamp(now_year_31)
    now_year_91_timestamp = date_string_to_timestamp(now_year_91)

    if now_time_timestamp < now_year_31_timestamp:
        return int(str(pre_year) + '1')
    elif now_year_31_timestamp <= now_time_timestamp < now_year_91_timestamp:
        return int(str(pre_year) + '2')
    else:
        return int(str(now_year) + '1')


if __name__ == '__main__':
    print(get_this_term())