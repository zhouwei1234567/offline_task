import json
import pickle


def parse_line(line, sep=','):
    assert len(sep) == 1 and sep not in ['\'', '"', '[', ']', '{', '}', '(', ')', '\t', '\n']
    rst = []
    bracket1 = 0
    bracket2 = 0
    bracket3 = 0
    in_quotation1 = False
    in_quotation2 = False
    tmp = ''
    for c in line:
        if c == sep and bracket1 == 0 and bracket2 == 0 and bracket3 == 0 and not in_quotation1 and not in_quotation2:
            rst.append(tmp.strip())
            tmp = ''
            continue
        elif c == ' ' and not tmp:
            continue
        elif c == '\'':
            in_quotation1 = not in_quotation1
        elif c == '"':
            in_quotation2 = not in_quotation2
        elif c == '[':
            bracket1 += 1
        elif c == ']':
            bracket1 -= 1
        elif c == '{':
            bracket2 += 1
        elif c == '}':
            bracket2 -= 1
        elif c == '(':
            bracket3 += 1
        elif c == ')':
            bracket3 -= 1
        elif c == '\n':
            break
        tmp += c
    rst.append(tmp.strip())
    return rst


class CSVreader(object):
    """Usage:
    ```
        with CSVreader(resp_file, primary_key=None, sep=',', header=None) as resp_reader:
            key = resp_reader.readline(accept_keys=[], except_keys=[])
            while key:
                ...
                key = resp_reader.readline(accept_keys=[], except_keys=[])
    ```
    """
    def __init__(self, filename: str, primary_key, sep=',', header=None):
        self.fp = open(filename, encoding='UTF-8')
        self._id = 0
        self.sep = sep
        if primary_key is not None:
            self.primary_key = str(primary_key).strip()
        else:
            self.primary_key = primary_key
        if header:
            self.header = self._parse_header(header)
        else:
            self.header = self._parse_header(self.fp.readline())
        self._dict = {} # todo 这个地方有内存泄露的风险，需要调用POP的方法，将每行数据吐出去

    def _parse_header(self, first_line):
        if isinstance(first_line, str):
            first_line = first_line.strip()
            if self.sep == '\t' or self.sep == ',':
                items = first_line.split(self.sep)
            else:
                items = parse_line(first_line, self.sep)
        elif isinstance(first_line, list):
            items = first_line
        else:
            raise ValueError("提取表头失败")
        found_prim_key = False
        self.keys = {}
        for idx, k in enumerate(items):
            self.keys[idx] = k.strip()
            if k == self.primary_key:
                found_prim_key = True
        if not found_prim_key and self.primary_key is not None:
            raise ValueError('没有找到主键 {}: {}'.format(self.primary_key, first_line))
        return items

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __getitem__(self, key):
        return self._dict[key]

    def __len__(self):
        return len(self._dict)

    def __iter__(self):
        return iter(self._dict)

    def close(self):
        fp = getattr(self, 'fp', None)
        if fp is not None:
            fp.close()

    def readline(self, accept_keys=None, except_keys=None, allow_dup_prim_key=False):
        line = self.fp.readline().strip("\n\r")
        # if len(line) < 10:
        #     line = self.fp.readline()
        if not line:
            return None
        if self.sep == '\t' or self.sep == ',':
            items = line.split(self.sep)
        else:
            items = parse_line(line, self.sep)
        prim = None
        vals = {}
        if accept_keys:
            accept_keys = set(accept_keys)
        if except_keys:
            except_keys = set(except_keys)
        for idx, itm in enumerate(items):
            itm = itm.strip()
            key = self.keys[idx]
            if key == self.primary_key:
                if itm is None:
                    raise ValueError('主键值不能为 None: {}'.format(line))
                prim = itm
            elif (accept_keys and key not in accept_keys) or (except_keys and key in except_keys):
                continue
            if itm == "NULL" or not itm:
                itm = "None"
            vals[key] = itm

        # When using self._id as primary key, it must start with 1
        if self.primary_key is None:
            self._id += 1
            self._dict[self._id] = vals
            return self._id
        elif prim is None:
            raise ValueError('没有找到主键 {}: {}'.format(self.primary_key, line))
        elif prim in self._dict and not allow_dup_prim_key:
            raise ValueError('主键重复: {} <--> {}'.format(self._dict[prim], vals))
        else:
            self._dict[prim] = vals
            return prim

    def pop(self, key):
        return self._dict.pop(key, None)

    def tolist(self, key, items=None, dtype=None):
        if not items:
            items = [self.keys[k] for k in self.keys]
        rst = []
        if key not in self._dict:
            return rst
        query = self._dict[key]
        if query is None:
            return rst
        for itm in items:
            if dtype is not None:
                insert = dtype(query[itm])
            else:
                insert = query[itm]
            rst.append(insert)
        return rst

    def readlines(self, n: int, accept_keys=None):
        rst = self.readline(accept_keys)
        while rst:
            rst = self.readline(accept_keys)

    def export_json(self, json_file: str, indent=4):
        with open(json_file, 'w', encoding='UTF-8') as fp:
            json.dump(self._dict, fp, ensure_ascii=False, indent=indent)

    def export_pickle(self, pickle_file: str):
        with open(pickle_file, 'wb') as fp:
            pickle.dump(self._dict, fp, protocol=pickle.HIGHEST_PROTOCOL)

    def import_data(self, data: dict):
        self._dict = {**self._dict, **data}

    @staticmethod
    def trans_none(info):
        if info == 'None' or info == 'NULL' or info == 'null':
            return None
        return info
