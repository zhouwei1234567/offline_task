"""project config"""


class Config(object):
    """
    配置基本
    @date 2018-08-02
    @author zhouwei
    """
    __slots__ = (
        "mysql_config",
        "mongo_config",
        "redis_cluster_config",
        "common_config",
        "script_config",
    )

    def __init__(
        self,
        mysql_config=None,
        mongo_config=None,
        redis_cluster_config=None,
        common_config: dict = None,
        script_config: dict = None,
    ):
        self.mysql_config = mysql_config
        self.mongo_config = mongo_config
        self.redis_cluster_config = redis_cluster_config
        self.common_config = common_config
        self.script_config = script_config


# mysql的配置类
class MysqlConfig(object):
    """
    mysql 配置
    @date 2018-08-02
    @author zhouwei
    """
    __slots__ = (
        "host",
        "port",
        "username",
        "password",
        "db_name",
        "pool_size",
        "max_overflow",
        "encoding",
    )

    def __init__(
        self,
        host,
        port,
        username,
        password,
        db_name,
        pool_size=3,
        max_overflow=0,
        encoding="utf-8",
    ):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name
        # 需要注意：由于python是多进程服务，最小连接数设置的是每个进程的最小连接数，小心把数据库链接打爆
        self.pool_size = pool_size
        self.encoding = encoding
        self.max_overflow = max_overflow

    def __call__(self, *args, **kwargs):
        return f"mysql+pymysql://{self.username}:{self.password}@{self.host}:{self.port}/{self.db_name}"


class MongoDBConfig(object):
    """
    mongo配置
    @date 2018-08-02
    @author zhouwei
    """
    __slots__ = (
        "url",
        "username",
        "password",
        "db_name",
        "max_pool_size",
        "min_pool_size",
        "auth_source",
        "max_idle_time_ms",
    )

    def __init__(
        self,
        url,
        username,
        password,
        max_pool_size=10,
        min_pool_size=5,
        auth_source="admin",
        max_idle_time_ms=600000,
    ):
        self.url = url
        self.username = username
        self.password = password
        self.max_pool_size = max_pool_size
        # 经过测试，min_pool_size = 0，默认开一个，min_pool_size = 1，默认开2个。结论：单进程连接数等于min_pool_size + 1
        # 需要注意：由于python是多进程服务，最小连接数设置的是每个进程的最小连接数，小心把数据库链接打爆
        self.min_pool_size = min_pool_size
        self.auth_source = auth_source
        self.max_idle_time_ms = max_idle_time_ms

    def __call__(self, *args, **kwargs):
        return "mongodb://" + self.url + "/" + "?authSource=" + self.auth_source

    def __repr__(self):
        return f"mongodb://{self.url}/?authoSource={self.auth_source}"


class RedisClusterConfig(object):
    """
    redis配置
    @date 2018-08-02
    @author zhouwei
    """
    __slots__ = ("startup_nodes", "max_connections")

    def __init__(self, startup_nodes, max_connections=3):
        self.startup_nodes = startup_nodes
        self.max_connections = max_connections
