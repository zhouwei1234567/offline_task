import os
import time
import platform
import getpass

from logging import FileHandler, StreamHandler, getLogger, Formatter, INFO
from .config import Config, MysqlConfig, MongoDBConfig, RedisClusterConfig
from pathlib import Path

"""
@date 2018-08-02
@author zhouwei
"""

# 运行环境变量常量设置，新的线上设备一定要配置环境变量running_environment
RUNNING_ENVIRONMENT_KEY = "running_environment"
RUNNING_ENVIRONMENT_MASTER = "master"
RUNNING_ENVIRONMENT_STAGING = "staging"
RUNNING_ENVIRONMENT_RELEASE = "release"
RUNNING_ENVIRONMENT = os.environ.get(
    RUNNING_ENVIRONMENT_KEY, default=RUNNING_ENVIRONMENT_MASTER
)

# 因为plan_task会在不同设备上部署，可能会出现某些盘的大小不一致，在线上则日志的路径可能会有差异
# 所以在这里做一个配置，如果环境变量里面有，则从环境变量里面取，否则，默认：/data/log/plan_task/
RUNNING_ENVIRONMENT_LOG_DIR_KEY = "plan_task_log_dir"
LOG_DIR = os.environ.get(RUNNING_ENVIRONMENT_LOG_DIR_KEY, default="/data/log/plan_task")

# log_dir，每天都建立一个新的文件夹，来存放日志
logger_dir = None
logger_dir_date = None
today = time.strftime("%Y%m%d", time.localtime())


# 工程的根目录
APP_ROOT_DIR = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../")

# 默认日志类，通常直接引用即可
logger = getLogger()
logging_format = Formatter(
    "%(asctime)s - %(levelname)s - %(module)s.%(filename)s.%(lineno)d - %(funcName)s - %(message)s"
)

def is_prodution():
    return RUNNING_ENVIRONMENT == RUNNING_ENVIRONMENT_RELEASE and is_linux()

"""下面都是配置信息"""
if RUNNING_ENVIRONMENT == RUNNING_ENVIRONMENT_RELEASE:
    # 线上环境配置，请谨慎修改
    from .release_app_config import app_config

    if not Path(LOG_DIR).exists() and not is_mac():
        raise Exception("plan_task log dir is not exists, the dir config: {}".format(LOG_DIR))

    # 设置日志地址
    logger_dir = (LOG_DIR if is_linux() and is_root_or_model_user() else os.path.expanduser("~"))
    logger_dir_date = os.path.join(logger_dir, today + "/")
    if not os.path.exists(logger_dir):
        os.makedirs(logger_dir)
    handler = FileHandler(logger_dir + "/application.log")
    handler.setLevel(INFO)
    handler.setFormatter(logging_format)
    logger.addHandler(handler)
    logger.setLevel(INFO)
    
elif RUNNING_ENVIRONMENT == RUNNING_ENVIRONMENT_STAGING:
    # staging的配置环境，因为线上没有staging，但开发中偶尔要用staging来调试，所以可以加载staging配置
    from .staging_app_config import app_config

    # 设置日志地址
    logger_dir = APP_ROOT_DIR + "/log/"
    logger_dir_date = (logger_dir + today + "/")  # 格式：/data/log/plan_task/当前的日期/script_dir_name
    if not os.path.exists(logger_dir):
        os.makedirs(logger_dir)
    handler = StreamHandler()
    handler.setLevel(INFO)
    handler.setFormatter(logging_format)
    logger.addHandler(handler)
    logger.setLevel(INFO)
    
else:
    # 测试环境配置
    from .test_app_config import app_config

    # 设置日志地址
    logger_dir = APP_ROOT_DIR + "/log/"
    logger_dir_date = (logger_dir + today + "/")  # 格式：/data/log/plan_task/当前的日期/script_dir_name
    if not os.path.exists(logger_dir):
        os.makedirs(logger_dir)
    handler = StreamHandler()
    handler.setLevel(INFO)
    handler.setFormatter(logging_format)
    logger.addHandler(handler)
    logger.setLevel(INFO)
"""上面都是配置信息"""


def get_comment_config_by_key(key):
    """
    获取通用的配置文件
    :param key:
    :return:
    """
    return app_config.common_config.get(key)


# 获取脚本的配置
def get_script_config_by_key(key):
    """
    获取脚本特有个的配置文件
    :param key:
    :return:
    """
    return app_config.script_config.get(key)

def get_logger_new(script_dir_name, name, file_name=None, start_date=None, root=False):
    """
    1. 生成一个新的logger，记录当前的业务日志
    2. 线上日志根路径：/data/log/plan_task，注意：线上环境需要提前建立好这个目录
    3. 测试日志根路径：当前工程的根目录下的log文件夹
    4. 建立的日志文件，路径为：/data/log/plan_task/{yyyy-MM-dd}/{script_dir_name}/{name}.log
    
    :param script_dir_name: 通常为plan_task的script文件夹的名称
    :param name: 具体的log文件的名称，根据自己需要定义即可
    :param file_name: 字段已经废弃，勿使用
    :param start_date: 字段已经废弃，勿使用
    :param root: 日志是否建立在更目录
    :return:
    """
    # 当file_name为None是，也name一致
    if file_name is None:
        file_name = name
    if root: # 线上的日志领为 /data/log/plan_task/20191212/脚本文件夹名称
        log_path = "{}".format(logger_dir)
    else:  # 线上的路径为 /data/log/plan_task
        log_path = "{}/{}/{}".format(logger_dir, today, script_dir_name)
    if not os.path.exists(log_path):
        os.makedirs(log_path)
    logger_new = getLogger(name)
    if RUNNING_ENVIRONMENT == RUNNING_ENVIRONMENT_RELEASE:
        handler_new = FileHandler("{}/{}.log".format(log_path, file_name))
        handler_new.setLevel(INFO)
        handler_new.setFormatter(logging_format)
    else:
        handler_new = StreamHandler()
        handler_new.setLevel(INFO)
        handler_new.setFormatter(logging_format)
    logger_new.addHandler(handler_new)
    logger_new.setLevel(INFO)
    logger_new.propagate = False  # 防止日志向上传播给它的父logger，然后调用父类的handle在处理一次日志
    return logger_new

# 获取各个脚本存放数据的目录，传入脚本的目录，生成目录
# demo: /data/log/plan_task/yyyy-MM-dd/script_dir_name
def get_data_dir(script_dir_name):
    _dir = logger_dir_date + script_dir_name
    if not os.path.exists(_dir):
        os.makedirs(_dir)
    return _dir
