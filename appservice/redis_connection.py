from . import app_config
from rediscluster import RedisCluster

__redis_db_cache = {}


# 公用的创建rediscluster的工厂
def create_redis_cluster_factory(redis_cluster_name, max_connections):
    if __redis_db_cache.get(redis_cluster_name) is not None:
        return __redis_db_cache.get(redis_cluster_name)

    redis_cluster_config = app_config.redis_cluster_config[redis_cluster_name]
    if max_connections is None:
        max_connections = redis_cluster_config.max_connections
    redis = RedisCluster(startup_nodes=redis_cluster_config.startup_nodes, max_connections=max_connections, decode_responses=True)
    if redis is not None:
        __redis_db_cache[redis_cluster_name] = redis
    return redis


# 创建smart-learning的redis链接
def create_test_cluster(max_connections=None) -> RedisCluster:
    return create_redis_cluster_factory('test', max_connections)
