from sqlalchemy.engine.result import ResultProxy
from pymysql import converters


class MysqlBase(object):

    charset: str = "ut-8"

    @staticmethod
    def escape_item(val, mapping=None):
        return converters.escape_item(val, MysqlBase.charset, mapping)

    @staticmethod
    def escape_sequence(val, mapping=None):
        return converters.escape_sequence(val, MysqlBase.charset, mapping)

    @staticmethod
    def escape_dict(val, mapping=None):
        return converters.escape_dict(val, MysqlBase.charset, mapping)

    @staticmethod
    def escape_set(val, mapping=None):
        return converters.escape_set(val, MysqlBase.charset, mapping)

    @staticmethod
    def escape_date(obj, mapping=None):
        return converters.escape_date(obj, mapping)

    @staticmethod
    def escape_time(obj, mapping=None):
        return converters.escape_time(obj, mapping)

    @staticmethod
    def escape_datetime(obj, mapping=None):
        return converters.escape_datetime(obj, mapping)

    @staticmethod
    def get_in_list_sql(values):
        sql = ""
        for val in values:
            sql = sql + str(MysqlBase.escape_item(val)) + ", "
        return sql.rstrip(", ")

    @staticmethod
    def to_object(class_name, result: ResultProxy):
        if result.rowcount < 1:
            return
        slots = class_name.__dict__["__slots__"]
        attr = set()
        for key in slots:
            attr.add(key)
        obj = class_name()
        row = result.fetchone()
        index = 0
        for key in result.keys():
            if attr.__contains__(key):
                setattr(obj, key, row[index])
            index = index + 1
        return obj

    @staticmethod
    def to_list_object(class_name, result: ResultProxy):
        if result.rowcount < 1:
            return list()
        slots = class_name.__dict__["__slots__"]
        attr = set()
        for key in slots:
            attr.add(key)
        rows = result.fetchall()
        obj_list = list()
        for row in rows:
            obj = class_name()
            index = 0
            for key in result.keys():
                if attr.__contains__(key):
                    setattr(obj, key, row[index])
                index = index + 1
            obj_list.append(obj)
        return obj_list
