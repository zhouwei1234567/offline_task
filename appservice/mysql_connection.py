from sqlalchemy import create_engine
from . import app_config

# mysql的链接初始化
_mysql_connections = {}


def create_mysql_db_engine(mysql_name):
    if _mysql_connections.get(mysql_name) is None:
        mysql_config = app_config.mysql_config
        mysql_db_config = mysql_config.get(mysql_name)
        _mysql_connections[mysql_name] = create_engine(
            mysql_db_config(),
            pool_size=mysql_db_config.pool_size,
            encoding=mysql_db_config.encoding,
            max_overflow=mysql_db_config.max_overflow,
        )


# 网校mysql
def get_test_mysql_db():
    create_mysql_db_engine("test")
    return _mysql_connections["test"]
