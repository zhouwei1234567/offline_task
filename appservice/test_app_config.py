from .config import Config, MysqlConfig, MongoDBConfig, RedisClusterConfig

"""
开发与测试环境配置
"""
app_config = Config(
    mysql_config={
        "test": MysqlConfig(
            "198.168.0.1", "3306", "test", "test", "test_db", 5
        )
    },
    mongo_config={
        "test": MongoDBConfig(
            "192.168.0.1:51101,192.168.0.1:51101",
            "test",
            "test",
            min_pool_size=0,
            max_pool_size=1,
        ),
    },
    redis_cluster_config={
        "test": RedisClusterConfig(
            [
                {"host": "192.168.0.1", "port": 6379},
                {"host": "192.168.0.1", "port": 6380},
                {"host": "192.168.0.2", "port": 6379},
                {"host": "192.168.0.2", "port": 6380},
            ],
            5,
        )
    },
    # 存放公用的一些配置
    common_config={

    },
    # 脚本的特殊配置，每个脚本一个map
    script_config={},
)
