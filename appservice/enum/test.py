from enum import Enum

'''
测评报告中的题目类型
'''
class Test(Enum):
    
    HELLO = ("你好", 1)
    
    WORLD = ("世界", 2)

    def get_name(self):
        return self.value[0]
    
    def get_value(self):
        return self.value[1]